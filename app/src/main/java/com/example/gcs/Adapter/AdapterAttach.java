package com.example.gcs.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gcs.Model.AttachFileModel;
import com.example.gcs.Model.JoinedCatModel;
import com.example.gcs.R;

import java.util.ArrayList;

public class AdapterAttach extends RecyclerView.Adapter<AdapterAttach.Viewholder> {

    private Context context;
    private final OnItemClickListener mListener;
    private ArrayList<AttachFileModel> fileModelArrayList;


    public AdapterAttach(Context context, ArrayList<AttachFileModel> fileModelArrayList, OnItemClickListener mListener) {
        this.context = context;
        this.mListener = mListener;
        this.fileModelArrayList = fileModelArrayList;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_attach, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(position);
            }
        });


        holder.attachTitle.setText(fileModelArrayList.get(position).getTitle());

    }

    @Override
    public int getItemCount() {
        return fileModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView attachTitle;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            attachTitle = itemView.findViewById(R.id.attachTitle);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }


}
