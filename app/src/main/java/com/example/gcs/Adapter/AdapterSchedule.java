package com.example.gcs.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.gcs.Model.MainCatModel;
import com.example.gcs.Model.ScheduleModel;
import com.example.gcs.R;

import java.util.ArrayList;

public class AdapterSchedule extends RecyclerView.Adapter<AdapterSchedule.Viewholder> {

    private Context context;
    private final OnItemClickListener mListener;
    private ArrayList<ScheduleModel> scheduleModelArrayList;


    public AdapterSchedule(Context context, ArrayList<ScheduleModel> scheduleModelArrayList, OnItemClickListener mListener) {
        this.context = context;
        this.mListener = mListener;
        this.scheduleModelArrayList = scheduleModelArrayList;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_schedule, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(position);
            }
        });


        holder.titleTV.setText(scheduleModelArrayList.get(position).getTitle());
        holder.event_period.setText(scheduleModelArrayList.get(position).getStart_time()
                + " To " + scheduleModelArrayList.get(position).getEnd_time());



    }

    @Override
    public int getItemCount() {
        return scheduleModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView event_period, titleTV;
        RelativeLayout relativeL;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            titleTV = itemView.findViewById(R.id.titleTV);
            event_period = itemView.findViewById(R.id.event_period);
            relativeL = itemView.findViewById(R.id.relativeL);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }


}
