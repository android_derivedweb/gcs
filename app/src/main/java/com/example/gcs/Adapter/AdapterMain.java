package com.example.gcs.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.gcs.Model.MainCatModel;
import com.example.gcs.R;

import java.util.ArrayList;

public class AdapterMain extends RecyclerView.Adapter<AdapterMain.Viewholder> {

    private Context context;
    private final OnItemClickListener mListener;
    private ArrayList<MainCatModel> mainCatModelArrayList;


    public AdapterMain(Context context, ArrayList<MainCatModel> mainCatModelArrayList, OnItemClickListener mListener) {
        this.context = context;
        this.mListener = mListener;
        this.mainCatModelArrayList = mainCatModelArrayList;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_main, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(position);
            }
        });


        holder.titleTV.setText(mainCatModelArrayList.get(position).getTitle());

        if (!mainCatModelArrayList.get(position).getPhoto().equals("null")) {
            Glide.with(context).load(mainCatModelArrayList.get(position).getPhoto()).into(holder.homeImage);
        }

    }

    @Override
    public int getItemCount() {
        return mainCatModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView titleTV;
        ImageView homeImage;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            titleTV = itemView.findViewById(R.id.titleTV);
            homeImage = itemView.findViewById(R.id.homeImage);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }


}
