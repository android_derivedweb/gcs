package com.example.gcs.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gcs.Activity.AttachWebImage;
import com.example.gcs.Model.ScheduleModel;
import com.example.gcs.Model.TopicModel;
import com.example.gcs.R;

import java.util.ArrayList;

public class AdapterTopic extends RecyclerView.Adapter<AdapterTopic.Viewholder> {

    private Context context;
    private final OnItemClickListener mListener;
    private ArrayList<TopicModel> topicModelArrayList;

    private AdapterJoinCat adapterJoinCat;
    private AdapterAttach adapterAttach;


    public AdapterTopic(Context context, ArrayList<TopicModel> topicModelArrayList, OnItemClickListener mListener) {
        this.context = context;
        this.mListener = mListener;
        this.topicModelArrayList = topicModelArrayList;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_topic, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(position);
            }
        });

        holder.titleTV.setText(topicModelArrayList.get(position).getTitle());


        holder.joinCatRec.setLayoutManager(new LinearLayoutManager(context));
        adapterJoinCat = new AdapterJoinCat(context, topicModelArrayList.get(position).getCatModelArrayList(), new AdapterJoinCat.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {

                if (!topicModelArrayList.get(position).getCatModelArrayList().get(pos).getPhoto().equals("null")){
                    context.startActivity(new Intent(context, AttachWebImage.class)
                            .putExtra("title", topicModelArrayList.get(position).getCatModelArrayList().get(pos).getTitle())
                            .putExtra("url", topicModelArrayList.get(position).getCatModelArrayList().get(pos).getPhoto()));
                } else {
                    Toast.makeText(context, "No file", Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.joinCatRec.setAdapter(adapterJoinCat);


        holder.attachFileRec.setLayoutManager(new LinearLayoutManager(context));
        adapterAttach = new AdapterAttach(context, topicModelArrayList.get(position).getAttachFileModelArrayList(), new AdapterAttach.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                if (topicModelArrayList.get(position).getAttachFileModelArrayList().get(item).getUrl().equals("null")) {
                    context.startActivity(new Intent(context, AttachWebImage.class)
                            .putExtra("title", topicModelArrayList.get(position).getAttachFileModelArrayList().get(item).getTitle())
                            .putExtra("url", topicModelArrayList.get(position).getAttachFileModelArrayList().get(item).getUrl()));
                } else {
                    Toast.makeText(context, "No file", Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.attachFileRec.setAdapter(adapterAttach);




    }

    @Override
    public int getItemCount() {
        return topicModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView titleTV;
        RelativeLayout relativeL;
        RecyclerView joinCatRec, attachFileRec;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            titleTV = itemView.findViewById(R.id.titleTV);
            relativeL = itemView.findViewById(R.id.relativeL);
            joinCatRec = itemView.findViewById(R.id.joinCatRec);
            attachFileRec = itemView.findViewById(R.id.attachFileRec);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }


}
