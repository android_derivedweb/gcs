package com.example.gcs.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gcs.Model.JoinedCatModel;
import com.example.gcs.Model.ScheduleModel;
import com.example.gcs.R;

import java.util.ArrayList;

public class AdapterJoinCat extends RecyclerView.Adapter<AdapterJoinCat.Viewholder> {

    private Context context;
    private final OnItemClickListener mListener;
    private ArrayList<JoinedCatModel> catModelArrayList;


    public AdapterJoinCat(Context context, ArrayList<JoinedCatModel> catModelArrayList, OnItemClickListener mListener) {
        this.context = context;
        this.mListener = mListener;
        this.catModelArrayList = catModelArrayList;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_join_cat, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(position);
            }
        });


        holder.tvJoined.setText(catModelArrayList.get(position).getTitle());

    }

    @Override
    public int getItemCount() {
        return catModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView tvJoined;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            tvJoined = itemView.findViewById(R.id.tvJoined);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }


}
