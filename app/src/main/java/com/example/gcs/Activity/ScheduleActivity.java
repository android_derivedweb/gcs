package com.example.gcs.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.gcs.Adapter.AdapterSchedule;
import com.example.gcs.Model.ScheduleModel;
import com.example.gcs.R;
import com.example.gcs.Utils.UserSession;
import com.example.gcs.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ScheduleActivity extends AppCompatActivity {

    private RecyclerView recItems;
    private AdapterSchedule adapterSchedule;

    private ArrayList<ScheduleModel> scheduleModelArrayList = new ArrayList<>();

    private UserSession session;

    private TextView currentDate, noEvents_TV;
    private RelativeLayout nxt_date, prv_date;

    private Date cDate;


    @SuppressLint("SimpleDateFormat")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        session = new UserSession(ScheduleActivity.this);


        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        currentDate = findViewById(R.id.currentDate);
        prv_date = findViewById(R.id.prv_date);
        nxt_date = findViewById(R.id.nxt_date);
        noEvents_TV = findViewById(R.id.noEvents_TV);


        recItems = findViewById(R.id.recItems);
        recItems.setLayoutManager(new LinearLayoutManager(this));
        adapterSchedule = new AdapterSchedule(this, scheduleModelArrayList, new AdapterSchedule.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {

            }
        });
        recItems.setAdapter(adapterSchedule);

        cDate = new Date();
        String fDate = new SimpleDateFormat("yyyy-MM-dd").format(cDate);

        currentDate.setText(new SimpleDateFormat("yyyy-MM-dd").format(cDate));


        nxt_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                c.setTime(cDate);
                c.add(Calendar.DATE, 1);
                cDate = c.getTime();

                currentDate.setText(new SimpleDateFormat("yyyy-MM-dd").format(cDate));

                prv_date.setVisibility(View.VISIBLE);

                getEvents(currentDate.getText().toString());
            }
        });


        prv_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                c.setTime(cDate);
                c.add(Calendar.DATE, -1);
                cDate = c.getTime();

                currentDate.setText(new SimpleDateFormat("yyyy-MM-dd").format(cDate));

                if (new SimpleDateFormat("yyyy-MM-dd").format(cDate)
                        .equals(new SimpleDateFormat("yyyy-MM-dd").format(new Date()))){
                    prv_date.setVisibility(View.GONE);
                }

                getEvents(currentDate.getText().toString());
            }
        });


        getEvents(fDate);

    }


    private void getEvents(String date) {
        final KProgressHUD progressDialog = KProgressHUD.create(ScheduleActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                session.BASEURL + "schedule?date=" + date,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        scheduleModelArrayList.clear();
                        noEvents_TV.setVisibility(View.GONE);

                        try {
                            Log.e("checkSchedule", new String(response.data) + "--");

                            JSONArray jsonArray = new JSONArray(new String(response.data));


                            for (int i = 0; i < jsonArray.length(); i++){
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                ScheduleModel model = new ScheduleModel();
                                model.setId(jsonObject1.getString("id"));
                                model.setTitle(jsonObject1.getString("title"));
                                model.setType(jsonObject1.getString("type"));

                                String inputPattern = "yyyy-MM-dd hh:mm:ss";
                                String outputPattern = "h:mm a";
                                SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                                SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                                Date timeStart = null;
                                Date timeEnd = null;
                                String strStartTime = null;
                                String strEndTime = null;

                                try {
                                    timeStart = inputFormat.parse(jsonObject1.getString("start"));
                                    timeEnd = inputFormat.parse(jsonObject1.getString("end"));
                                    strStartTime = outputFormat.format(timeStart);
                                    strEndTime = outputFormat.format(timeEnd);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                model.setStart_time(strStartTime);
                                model.setEnd_time(strEndTime);




                                scheduleModelArrayList.add(model);
                            }

                            adapterSchedule.notifyDataSetChanged();


                        } catch (Exception e) {

                            Toast.makeText(ScheduleActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            switch(error.networkResponse.statusCode){
                                case 404:
                                    scheduleModelArrayList.clear();
                                    adapterSchedule.notifyDataSetChanged();
                                    noEvents_TV.setVisibility(View.VISIBLE);
                                    break;
                            }

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(ScheduleActivity.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                                //Additional cases
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(ScheduleActivity.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(ScheduleActivity.this, "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
             /*   params.put("username", userNameET.getText().toString());
                params.put("password", passwordET.getText().toString());
                params.put("fullname", fullNameET.getText().toString());
                params.put("device_type", "android");
                params.put("device_token", android_id);*/

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //     params.put("Accept", "application/json");
                //         params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(ScheduleActivity.this).add(volleyMultipartRequest);
    }




}