package com.example.gcs.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.gcs.Model.MainCatModel;
import com.example.gcs.Model.MainSubCatModel;
import com.example.gcs.R;
import com.example.gcs.Utils.UserSession;
import com.example.gcs.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HomeActivity extends AppCompatActivity {

    private TextView businessTV, newsTV, photosTV, sessionInfoTV, feedbackTV, spiritualTV, stCroixTV, liveStreamTV;

    private UserSession session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        session = new UserSession(HomeActivity.this);


        businessTV = findViewById(R.id.businessTV);
        newsTV = findViewById(R.id.newsTV);
        photosTV = findViewById(R.id.photosTV);
        sessionInfoTV = findViewById(R.id.sessionInfoTV);
        feedbackTV = findViewById(R.id.feedbackTV);
        spiritualTV = findViewById(R.id.spiritualTV);
        stCroixTV = findViewById(R.id.stCroixTV);
        liveStreamTV = findViewById(R.id.liveStreamTV);


        businessTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getMainCat("2", "Business Session");

            }
        });

        newsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getMainCat("3", "News");

            }
        });

        photosTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                getMainCat("4", "Photos");

            }
        });

        sessionInfoTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getMainCat("11", "Session Information");

            }
        });

        feedbackTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getMainCat("12", "Feedback");

            }
        });

        spiritualTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getMainCat("13", "Spiritual Inspiration");

            }
        });

        stCroixTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getMainCat("14", "St. Croix");

            }
        });

        liveStreamTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getMainCat("15", "Live Stream");
            }
        });


    }


    private void getMainCat(String id, String title) {
        final KProgressHUD progressDialog = KProgressHUD.create(HomeActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                session.BASEURL + "categories/" + id,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            Log.e("checkCATMain", new String(response.data) + "--");


                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            if (jsonObject.getString("msg").equals("There is no data")){
                                startActivity(new Intent(HomeActivity.this, TopicActivity.class)
                                        .putExtra("categoryID", id)
                                        .putExtra("title", title));
                            } else {

                                startActivity(new Intent(HomeActivity.this, MainActivity.class)
                                        .putExtra("id", id));


                            }



                        } catch (Exception e) {

                            Toast.makeText(HomeActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(HomeActivity.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                                //Additional cases
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(HomeActivity.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(HomeActivity.this, "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
             /*   params.put("username", userNameET.getText().toString());
                params.put("password", passwordET.getText().toString());
                params.put("fullname", fullNameET.getText().toString());
                params.put("device_type", "android");
                params.put("device_token", android_id);*/

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //     params.put("Accept", "application/json");
                //         params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(HomeActivity.this).add(volleyMultipartRequest);
    }



}