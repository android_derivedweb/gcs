package com.example.gcs.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.gcs.Adapter.AdapterTopic;
import com.example.gcs.Model.AttachFileModel;
import com.example.gcs.Model.JoinedCatModel;
import com.example.gcs.Model.MainCatModel;
import com.example.gcs.Model.MainSubCatModel;
import com.example.gcs.Model.TopicModel;
import com.example.gcs.R;
import com.example.gcs.Utils.UserSession;
import com.example.gcs.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TopicActivity extends AppCompatActivity {

    private TextView titleSub;
    private RecyclerView recItems;

    private UserSession session;
    private ArrayList<TopicModel> topicModelArrayList = new ArrayList<>();
    private AdapterTopic adapterTopic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic);

        session = new UserSession(TopicActivity.this);

        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        titleSub = findViewById(R.id.titleSub);


        String categoryID = getIntent().getStringExtra("categoryID");
        titleSub.setText(getIntent().getStringExtra("title"));


        recItems = findViewById(R.id.recItems);
        recItems.setLayoutManager(new LinearLayoutManager(this));
        adapterTopic = new AdapterTopic(this, topicModelArrayList, new AdapterTopic.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {

            }
        });
        recItems.setAdapter(adapterTopic);



        getTopics(categoryID);

    }


    private void getTopics(String categoryID) {
        final KProgressHUD progressDialog = KProgressHUD.create(TopicActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                session.BASEURL + "topic/" + categoryID,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            Log.e("checkTopics", new String(response.data) + "--");

                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            JSONArray jsonArray = jsonObject.getJSONArray("topic");

                            for (int i = 0; i < jsonArray.length(); i++){
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                TopicModel model = new TopicModel();
                                model.setId(jsonObject1.getString("id"));
                                model.setTitle(jsonObject1.getString("title"));
                                model.setDetails(jsonObject1.getString("details"));
                                model.setDate(jsonObject1.getString("date"));
                                model.setPhoto_file(jsonObject1.getString("photo_file"));

                                ArrayList<JoinedCatModel> catModelArrayList = new ArrayList<>();
                                JSONArray array = jsonObject1.getJSONArray("Joined_categories");
                                for (int j = 0; j < array.length(); j++){
                                    JSONObject object = array.getJSONObject(j);

                                    JoinedCatModel catModel = new JoinedCatModel();
                                    catModel.setId(object.getString("id"));
                                    catModel.setTitle(object.getString("title"));
                                    catModel.setPhoto(object.getString("photo"));
                                    catModelArrayList.add(catModel);
                                }

                                ArrayList<AttachFileModel> fileModelArrayList = new ArrayList<>();
                                JSONArray arrayAttach = jsonObject1.getJSONArray("attach_files");
                                for (int j = 0; j < arrayAttach.length(); j++){
                                    JSONObject object = arrayAttach.getJSONObject(j);

                                    AttachFileModel fileModel = new AttachFileModel();
                                    fileModel.setId(object.getString("id"));
                                    fileModel.setTitle(object.getString("title"));
                                    fileModel.setUrl(object.getString("url"));
                                    fileModelArrayList.add(fileModel);
                                }


                                model.setCatModelArrayList(catModelArrayList);
                                model.setAttachFileModelArrayList(fileModelArrayList);

                                topicModelArrayList.add(model);
                            }

                            adapterTopic.notifyDataSetChanged();


                        } catch (Exception e) {

                            Toast.makeText(TopicActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(TopicActivity.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }

                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(TopicActivity.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(TopicActivity.this, "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
        //        params.put("username", userNameET.getText().toString());

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //     params.put("Accept", "application/json");
                //         params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(TopicActivity.this).add(volleyMultipartRequest);
    }


}