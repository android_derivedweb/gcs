package com.example.gcs.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.gcs.Adapter.AdapterMain;
import com.example.gcs.Model.MainCatModel;
import com.example.gcs.R;

import java.util.ArrayList;

public class MainSubActivity extends AppCompatActivity {

    private TextView titleSub;
    private RecyclerView recItems;
    private AdapterMain adapterMain;

    private ArrayList<MainCatModel> mainCatModelArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_sub);


        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        titleSub = findViewById(R.id.titleSub);

        ArrayList<String> IDs = new ArrayList<>();
        ArrayList<String> TITLEs = new ArrayList<>();
        ArrayList<String> PHOTOs = new ArrayList<>();

        IDs = getIntent().getStringArrayListExtra("IDs");
        TITLEs = getIntent().getStringArrayListExtra("TITLEs");
        PHOTOs = getIntent().getStringArrayListExtra("PHOTOs");

        titleSub.setText(getIntent().getStringExtra("title"));


        for (int i = 0; i < IDs.size(); i++){
            MainCatModel mainCatModel = new MainCatModel();
            mainCatModel.setId(IDs.get(i));
            mainCatModel.setTitle(TITLEs.get(i));
            mainCatModel.setPhoto(PHOTOs.get(i));

            mainCatModelArrayList.add(mainCatModel);
        }

        Log.e("dataSUB", IDs.get(0) + "--" + TITLEs.get(0) + "--" + PHOTOs.get(0));


        recItems = findViewById(R.id.recItems);
        recItems.setLayoutManager(new GridLayoutManager(MainSubActivity.this, 2));
        adapterMain = new AdapterMain(MainSubActivity.this, mainCatModelArrayList, new AdapterMain.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {

                startActivity(new Intent(MainSubActivity.this, TopicActivity.class)
                        .putExtra("categoryID", mainCatModelArrayList.get(pos).getId())
                        .putExtra("title", mainCatModelArrayList.get(pos).getTitle()));
            }
        });
        recItems.setAdapter(adapterMain);

    }


}