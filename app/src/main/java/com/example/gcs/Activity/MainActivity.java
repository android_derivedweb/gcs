package com.example.gcs.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.example.gcs.Adapter.AdapterMain;
import com.example.gcs.Model.MainCatModel;
import com.example.gcs.Model.MainSubCatModel;
import com.example.gcs.R;
import com.example.gcs.Utils.UserSession;
import com.example.gcs.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recItems;
    private TextView titleMain;

    private AdapterMain adapterMain;

    private ArrayList<MainCatModel> mainCatModelArrayList = new ArrayList<>();

    private UserSession session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        session = new UserSession(MainActivity.this);

        titleMain = findViewById(R.id.titleMain);


        findViewById(R.id.backBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        recItems = findViewById(R.id.recItems);
        recItems.setLayoutManager(new GridLayoutManager(MainActivity.this, 2));
        adapterMain = new AdapterMain(MainActivity.this, mainCatModelArrayList, new AdapterMain.OnItemClickListener() {
            @Override
            public void onItemClick(int pos) {

                if (mainCatModelArrayList.get(pos).getTitle().equals("Schedule")){
                    startActivity(new Intent(MainActivity.this, ScheduleActivity.class));

                } else if (!mainCatModelArrayList.get(pos).getSub_categories_count().equals("0")){

                    ArrayList<String> IDs = new ArrayList<>();
                    ArrayList<String> TITLEs = new ArrayList<>();
                    ArrayList<String> PHOTOs = new ArrayList<>();

                    for (int i = 0; i < mainCatModelArrayList.get(pos).getMainSubCatModelArrayList().size(); i++){
                        IDs.add(mainCatModelArrayList.get(pos).getMainSubCatModelArrayList().get(i).getId());
                        TITLEs.add(mainCatModelArrayList.get(pos).getMainSubCatModelArrayList().get(i).getTitle());
                        PHOTOs.add(mainCatModelArrayList.get(pos).getMainSubCatModelArrayList().get(i).getPhoto());
                    }

                    Intent intent = new Intent(MainActivity.this, MainSubActivity.class);
                    intent.putStringArrayListExtra("IDs", IDs);
                    intent.putStringArrayListExtra("TITLEs", TITLEs);
                    intent.putStringArrayListExtra("PHOTOs", PHOTOs);
                    intent.putExtra("title", mainCatModelArrayList.get(pos).getTitle());
                    startActivity(intent);

                } else if (mainCatModelArrayList.get(pos).getSub_categories_count().equals("0")){
                    startActivity(new Intent(MainActivity.this, TopicActivity.class)
                            .putExtra("categoryID", mainCatModelArrayList.get(pos).getId())
                            .putExtra("title", mainCatModelArrayList.get(pos).getTitle()));
                }

            }
        });
        recItems.setAdapter(adapterMain);


        getMainCat(getIntent().getStringExtra("id"));


    }


    private void getMainCat(String id) {
        final KProgressHUD progressDialog = KProgressHUD.create(MainActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET,
                session.BASEURL + "categories/" + id,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        try {
                            Log.e("checkCATMain", new String(response.data) + "--");


                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            titleMain.setText(jsonObject.getString("section_title"));

                            JSONArray jsonArray = jsonObject.getJSONArray("categories");

                            for (int i = 0; i < jsonArray.length(); i++){
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                MainCatModel blogModel = new MainCatModel();
                                blogModel.setId(jsonObject1.getString("id"));
                                blogModel.setTitle(jsonObject1.getString("title"));
                                blogModel.setPhoto(jsonObject1.getString("photo"));
                                blogModel.setSub_categories_count(jsonObject1.getString("sub_categories_count"));

                                ArrayList<MainSubCatModel> subCatModelArrayList = new ArrayList<>();

                                JSONArray array = jsonObject1.getJSONArray("sub_categories");
                                for (int j = 0; j < array.length(); j++){
                                    JSONObject object = array.getJSONObject(j);

                                    MainSubCatModel subCatModel = new MainSubCatModel();
                                    subCatModel.setId(object.getString("id"));
                                    subCatModel.setTitle(object.getString("title"));
                                    subCatModel.setPhoto(object.getString("photo"));

                                    subCatModelArrayList.add(subCatModel);
                                }

                                blogModel.setMainSubCatModelArrayList(subCatModelArrayList);

                                mainCatModelArrayList.add(blogModel);
                            }

                            if (id.equals("2")) {
                                MainCatModel blogModel = new MainCatModel();
                                blogModel.setId("");
                                blogModel.setTitle("Schedule");
                                blogModel.setPhoto("");
                                mainCatModelArrayList.add(blogModel);
                            }


                            adapterMain.notifyDataSetChanged();


                        } catch (Exception e) {

                            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError){

                            if(error.networkResponse != null && error.networkResponse.data != null){
                                switch(error.networkResponse.statusCode){
                                    case 500:
                                        String json = new String(error.networkResponse.data);
                                        json = session.trimMessage(json, "message");
                                        if(json != null) {
                                            Toast.makeText(MainActivity.this, json, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                                //Additional cases
                            }
                        }
                        else if (error instanceof TimeoutError)
                            Toast.makeText(MainActivity.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(MainActivity.this, "Bad Network Connection", Toast.LENGTH_LONG).show();

                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
             /*   params.put("username", userNameET.getText().toString());
                params.put("password", passwordET.getText().toString());
                params.put("fullname", fullNameET.getText().toString());
                params.put("device_type", "android");
                params.put("device_token", android_id);*/

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //     params.put("Accept", "application/json");
       //         params.put("Authorization", "Bearer " + session.getAPITOKEN());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };

        volleyMultipartRequest.setShouldRetryServerErrors(true);

        Volley.newRequestQueue(MainActivity.this).add(volleyMultipartRequest);
    }





}
