package com.example.gcs.Model;

import java.util.ArrayList;

public class MainCatModel {

    private String id;
    private String title;
    private String photo;
    private String sub_categories_count;
    private ArrayList<MainSubCatModel> mainSubCatModelArrayList;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getSub_categories_count() {
        return sub_categories_count;
    }

    public void setSub_categories_count(String sub_categories_count) {
        this.sub_categories_count = sub_categories_count;
    }

    public ArrayList<MainSubCatModel> getMainSubCatModelArrayList() {
        return mainSubCatModelArrayList;
    }

    public void setMainSubCatModelArrayList(ArrayList<MainSubCatModel> mainSubCatModelArrayList) {
        this.mainSubCatModelArrayList = mainSubCatModelArrayList;
    }
}
