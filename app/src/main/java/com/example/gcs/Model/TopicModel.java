package com.example.gcs.Model;

import java.util.ArrayList;

public class TopicModel {

    private String id;
    private String title;
    private String details;
    private String date;
    private String photo_file;
    private ArrayList<JoinedCatModel> catModelArrayList;
    private ArrayList<AttachFileModel> attachFileModelArrayList;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<JoinedCatModel> getCatModelArrayList() {
        return catModelArrayList;
    }

    public void setCatModelArrayList(ArrayList<JoinedCatModel> catModelArrayList) {
        this.catModelArrayList = catModelArrayList;
    }

    public ArrayList<AttachFileModel> getAttachFileModelArrayList() {
        return attachFileModelArrayList;
    }

    public void setAttachFileModelArrayList(ArrayList<AttachFileModel> attachFileModelArrayList) {
        this.attachFileModelArrayList = attachFileModelArrayList;
    }

    public String getPhoto_file() {
        return photo_file;
    }

    public void setPhoto_file(String photo_file) {
        this.photo_file = photo_file;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
