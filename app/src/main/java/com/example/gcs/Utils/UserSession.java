package com.example.gcs.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

public class UserSession {

    SharedPreferences sharedPreferences;

    SharedPreferences.Editor editor;

    Context context;

    int PRIVATE_MODE = 0;

    public String BASEURL = "https://nccsession.org/api/v1/";
    public String ANDROID_APP_VERSION = "1.0";

    private static final String PREF_NAME = "UserSessionPref";


    private static final String IS_LOGIN = "IsLoggedIn";
    private final String USER_ID = "User_id";
    private final String DEVICE_TOKEN = "device_token";
    private final String APITOKEN = "api_token";

    private final String TOKEN_TYPE = "token_type";



    public UserSession(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }


    public void setIsLogin(boolean b) {
        editor.putBoolean(IS_LOGIN, b);
        editor.commit();
    }


    public boolean logout() {
        return sharedPreferences.edit().clear().commit();
    }



    public void setAPITOKEN(String apitoken) {
        editor.putString(APITOKEN, apitoken);
        editor.commit();
    }

    public String getAPITOKEN() {
        return sharedPreferences.getString(APITOKEN, "");
    }



    public void setTOKEN_TYPE(String token_type) {
        editor.putString(TOKEN_TYPE, token_type);
        editor.commit();
    }

    public String getTOKEN_TYPE() {
        return sharedPreferences.getString(TOKEN_TYPE, "");
    }




    public void setUSER_ID(String user_id) {
        editor.putString(USER_ID, user_id);
        editor.commit();
    }

    public String getUSER_ID() {
        return sharedPreferences.getString(USER_ID, "");
    }





    public String trimMessage(String json, String key){
        String trimmedString = null;
        try{
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch(JSONException e){
            e.printStackTrace();
            return null;
        }
        return trimmedString;
    }


}
